/*
@author Christopher Wong

 */

package controller;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.*;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;
    private Text[]      letters;     // fill this up with all the letters
    private Button      hint;        // hint button
    private boolean     hintUsed;    // tells me if hint has been used yet or not
    private ArrayList<Shape> hangmanParts; //array list of hangman parts


    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        hintUsed = false;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox alreadyGuessed = gameWorkspace.getAlreadyGuessed();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        HBox hintButtonHolder = gameWorkspace.getHintButtonHolder();
        BorderPane figurePane = gameWorkspace.getFigurePane();
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        hintButtonHolder.setMinHeight(26);
        hintButtonHolder.setPadding(new Insets(1,0,0,0));
        hintButtonHolder.setAlignment(Pos.CENTER);
        initHintButton(hintButtonHolder, gameWorkspace);
        initWordGraphics(guessedLetters, alreadyGuessed);
        initHangmanGraphics(figurePane);
        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (!success){
                String word = gamedata.getTargetWord();
                for(int i = 0; i< word.length(); i++){
                    if(!progress[i].isVisible()){
                        progress[i].setFill(Color.RED);
                        progress[i].setVisible(true);
                        if(hint != null){
                            hint.setDisable(true);
                        }
                    }
                }
            }
            if (dialog.isShowing())
                dialog.toFront();
            else {
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
                if(hint != null){
                    hint.setDisable(true);
                }
            }
        });
    }

    private void initWordGraphics(HBox guessedLetters, HBox alreadyGuessed) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        letters = new Text[26];
        char[] annoyance  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        for(int j = 0; j < annoyance.length; j++){
            letters[j] = new Text(Character.toString(annoyance[j]));
            letters[j].setFont(Font.font ("Verdana", 25));
            letters[j].setFill(Color.WHITE);
            letters[j].setVisible(true);

            alreadyGuessed.getChildren().addAll(letters[j]);
            alreadyGuessed.getChildren().addAll(square(letters[j]));
        }
        alreadyGuessed.setPadding(new Insets(10, 2, 0, 2));
        alreadyGuessed.setSpacing(1);

        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);

            guessedLetters.getChildren().addAll(progress[i]);
            guessedLetters.getChildren().addAll(rectangle(progress[i]));
        }
    }

    private void initHangmanGraphics(BorderPane figurePane) {
        Circle circle = new Circle();
        circle.setRadius(20);
        circle.setCenterX(475);
        circle.setCenterY(80);
        circle.setVisible(false);

        Line neckBody = new Line();
        neckBody.setStartX(475);
        neckBody.setEndX(475);
        neckBody.setStartY(100);
        neckBody.setEndY(150);
        neckBody.setStrokeWidth(5);
        neckBody.setVisible(false);

        Line leftArm = new Line();
        leftArm.setStartX(475);
        leftArm.setEndX(450);
        leftArm.setStartY(120);
        leftArm.setEndY(135);
        leftArm.setStrokeWidth(5);
        leftArm.setVisible(false);

        Line leftLeg = new Line();
        leftLeg.setStartX(475);
        leftLeg.setEndX(460);
        leftLeg.setStartY(150);
        leftLeg.setEndY(190);
        leftLeg.setStrokeWidth(5);
        leftLeg.setVisible(false);

        Line rightLeg = new Line();
        rightLeg.setStartX(475);
        rightLeg.setEndX(490);
        rightLeg.setStartY(150);
        rightLeg.setEndY(190);
        rightLeg.setStrokeWidth(5);
        rightLeg.setVisible(false);

        Line rightArm = new Line();
        rightArm.setStartX(475);
        rightArm.setEndX(500);
        rightArm.setStartY(120);
        rightArm.setEndY(135);
        rightArm.setStrokeWidth(5);
        rightArm.setVisible(false);

        Line lineTop = new Line();
        lineTop.setStartX(325);
        lineTop.setEndX(475);
        lineTop.setStartY(20);
        lineTop.setEndY(20);
        lineTop.setStrokeWidth(10);
        lineTop.setVisible(false);

        Line lineBottom = new Line();
        lineBottom.setStartX(325);
        lineBottom.setEndX(525);
        lineBottom.setStartY(270);
        lineBottom.setEndY(270);
        lineBottom.setStrokeWidth(10);
        lineBottom.setVisible(false);

        Line rope = new Line();
        rope.setStartX(475);
        rope.setEndX(475);
        rope.setStartY(20);
        rope.setEndY(60);
        rope.setStrokeWidth(5);
        rope.setVisible(false);

        Line pole = new Line();
        pole.setStartX(325);
        pole.setEndX(325);
        pole.setStartY(20);
        pole.setEndY(270);
        pole.setStrokeWidth(10);
        pole.setVisible(false);

        Canvas canvas = new Canvas(200, 280);

        figurePane.setCenter(canvas);

        hangmanParts = new ArrayList<Shape>();
        hangmanParts.add((Shape)lineBottom);
        hangmanParts.add((Shape)pole);
        hangmanParts.add((Shape)lineTop);
        hangmanParts.add((Shape)rope);
        hangmanParts.add((Shape)circle);
        hangmanParts.add((Shape)neckBody);
        hangmanParts.add((Shape)leftArm);
        hangmanParts.add((Shape)rightArm);
        hangmanParts.add((Shape)leftLeg);
        hangmanParts.add((Shape)rightLeg);

        figurePane.getChildren().addAll(lineBottom, pole, lineTop, rope, circle, neckBody, leftArm, rightArm,
                                        leftLeg, rightLeg);

    }

    private void initHintButton(HBox hintButtonHolder, Workspace workspace) {
        char [] ourWord = gamedata.getTargetWord().toCharArray();

        HashSet<Character> uniqueLetters = new HashSet<Character>();

        for(int i = 0; i < ourWord.length; i++){
            uniqueLetters.add(ourWord[i]);
        }

        if(uniqueLetters.size() > 7){                   // only have the hint button if word has > 7 unique letters
            hint = new Button("Hint");
            workspace.setHintButton(hint);
        }
    }

    public void handleHintRequest(){

            hintUsed = true;
            hint.setDisable(hintUsed);
            gamedata.setHintUsed(hintUsed);

            setGameState(GameState.INITIALIZED_MODIFIED);

            char [] word = gamedata.getTargetWord().toCharArray();      // our current game word
            int remainingGuesses = gamedata.getRemainingGuesses();        // our remaining guesses number

            HashSet<Character>goodGuesses = (HashSet)gamedata.getGoodGuesses(); // char hashset of our good guesses
            ArrayList<Character>availableForHint = new ArrayList<Character>();      // char hashset for our hint possibility

            for(int i = 0; i < word.length; i++){
                if(!(goodGuesses.contains(word[i]))){
                    availableForHint.add(word[i]);
                }
            }

            // now we have all our possible letters for hints inside hashset

                int random = new Random().nextInt(availableForHint.size()); // helps us pick a random index from
                                                                            // our hash array of available letters

                char hintLetter = availableForHint.get(random);

                gamedata.addGoodGuess(hintLetter);

                for(int i = 0; i < progress.length; i++){
                    if (progress[i].getText().equals(hintLetter + "")){
                        progress[i].setVisible(true);
                        discovered++;
                    }
                }

        if(remainingGuesses > 1){
            gamedata.setRemainingGuesses(remainingGuesses-1);
            success = (discovered == progress.length);
            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
            hangmanParts.get(gamedata.getBadGuesses().size()).setVisible(true);

            if (success) {
                end();
            }

                letters[hintLetter - 97].setVisible(false);

            }

    }



    private HBox square (Text text) {
        final HBox hbox = new HBox();
        hbox.setMinWidth(30);
        hbox.setMinHeight(30);
        hbox.getChildren().addAll(text);
        hbox.setStyle("-fx-background-color: #FF00FF;");
        hbox.setAlignment(Pos.CENTER);
        hbox.setVisible(true);
        return hbox ;
    }

    private HBox rectangle (Text text) {
        final HBox hbox = new HBox();
        hbox.setMinWidth(15);
        hbox.setMinHeight(15);
        hbox.getChildren().add(text);
        hbox.setStyle("-fx-border-color: black;");
        hbox.setAlignment(Pos.CENTER);
        hbox.setVisible(true);
        return hbox ;
    }

    public void play() {
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                            char guess = event.getCharacter().charAt(0);
                            if(Character.isLetter(guess)){
                                guess = Character.toLowerCase(guess);
                            if (!alreadyGuessed(guess)) {
                                boolean goodguess = false;
                                for (int i = 0; i < progress.length; i++) {
                                    if (gamedata.getTargetWord().charAt(i) == guess) {
                                        progress[i].setVisible(true);
                                        letters[guess - 97].setVisible(false);
                                        gamedata.addGoodGuess(guess);
                                        goodguess = true;
                                        discovered++;
                                        setGameState(GameState.INITIALIZED_MODIFIED);
                                    }
                                }
                                if (!goodguess) {
                                    gamedata.addBadGuess(guess);
                                    setGameState(GameState.INITIALIZED_MODIFIED);
                                    letters[guess - 97].setVisible(false);

                                    for(int i = 0; i < gamedata.getBadGuesses().size(); i++){
                                        hangmanParts.get(i).setVisible(true);
                                    }
                                    if(hintUsed && gamedata.getBadGuesses().size() < 10){
                                        hangmanParts.get(gamedata.getBadGuesses().size()).setVisible(true);
                                    }
                                }

                                success = (discovered == progress.length);
                                remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                            }

                        }

                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void restoreGUI() {
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        BorderPane figurePane = gameWorkspace.getFigurePane();
        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        HBox alreadyGuessed = gameWorkspace.getAlreadyGuessed();
        HBox hintButtonHolder = gameWorkspace.getHintButtonHolder();
        hintButtonHolder.setPadding(new Insets(10,10,10,10));
        hintButtonHolder.setMinHeight(26);
        hintButtonHolder.setPadding(new Insets(1,0,0,0));
        hintButtonHolder.setAlignment(Pos.CENTER);
        restoreWordGraphics(guessedLetters, alreadyGuessed);
        restoreHangmanGraphics(figurePane);
        restoreHintButton(hintButtonHolder, gameWorkspace);

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();

        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        success = false;
        hintUsed = false;
        play();
    }

    private void restoreHintButton(HBox hintButtonHolder, Workspace gameWorkspace) {
        initHintButton(hintButtonHolder, gameWorkspace);
        hint.setDisable(gamedata.isHintUsed());
    }

    private void restoreWordGraphics(HBox guessedLetters, HBox alreadyGuessed) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        letters = new Text[26];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            if (progress[i].isVisible()) {
                discovered++;
            }
            guessedLetters.getChildren().addAll(progress[i]);
            guessedLetters.getChildren().addAll(rectangle(progress[i]));
        }

        char[] annoyance  = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

        for(int j = 0; j < annoyance.length; j++){
            letters[j] = new Text(Character.toString(annoyance[j]));
            letters[j].setFont(Font.font ("Verdana", 25));
            letters[j].setFill(Color.WHITE);

            letters[j].setVisible(!(gamedata.getGoodGuesses().contains(letters[j].getText().toLowerCase().charAt(0)) ||
                    gamedata.getBadGuesses().contains(letters[j].getText().toLowerCase().charAt(0))));

            alreadyGuessed.getChildren().addAll(letters[j]);
            alreadyGuessed.getChildren().addAll(square(letters[j]));
        }
        alreadyGuessed.setPadding(new Insets(10, 2, 0, 2));
        alreadyGuessed.setSpacing(1);
    }

    private void restoreHangmanGraphics(BorderPane figurePane){
        initHangmanGraphics(figurePane);

        for(int i = 0; i < gamedata.getBadGuesses().size(); i++){
            hangmanParts.get(i).setVisible(true);
        }


    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            setGameState(GameState.INITIALIZED_UNMODIFIED);
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());
            restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
